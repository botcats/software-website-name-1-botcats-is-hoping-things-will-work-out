package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;

import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.TELEOP;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="TeleOpMode", group="Linear Opmode")
public class TeleOp extends BaseOpMode {


    @Override
    public void runOpMode() {

        // Sets OpMode Type
        type = TELEOP;

        // Creates a timer
        runtime = new ElapsedTime();

        // Creates the logger
        debugLog = new BotcatsLog(runtime, "TeleOp", BotcatsLog.LogType.DEBUG, loggingEnabled);
        debugLog.openLog();

        // Runs the initialize() function from BaseOpMode, starting modules
        initialize();

        // Waits for driver to press play
        waitForStart();

        // If logger is enabled, write to the log that TeleOp is starting
        if (loggingEnabled) {
            debugLog.addDbgMessage(BotcatsLog.DbgLevel.INFO, "OpMode", "---------- Starting TeleOp ----------");
        }

        // Resets the timer
        runtime.reset();

        while (opModeIsActive()) {

            // If the stick is pushed in any direction, the drive will move accordingly.
            if (Math.abs(gamepad1.left_stick_y) > 0.1) {
                drive.move(gamepad1.left_stick_y);
            }

            // If either trigger is pressed, the drive will turn.
            else if (gamepad1.left_trigger > .1){
                drive.turnLeft(gamepad1.left_trigger);
            }
            else if (gamepad1.right_trigger > .1){
                drive.turnRight(gamepad1.right_trigger);
            }

            // If no controls are touched, the drive will stop.
            else {
                drive.stopAll();
            }

            // If the driver presses stop on the phone, run shutdown() function from BaseOpMode
            if (isStopRequested()) {
                // If logger is enabled, write to the log that TeleOp is stopping
                if (loggingEnabled) {
                    debugLog.addDbgMessage(
                            BotcatsLog.DbgLevel.INFO, "OpMode", "---------- Stopping TeleOp ----------");
                }
                shutdown();
            }
        }
    }
}
