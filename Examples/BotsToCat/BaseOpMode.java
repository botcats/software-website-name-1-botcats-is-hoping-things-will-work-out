package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.hardware.Drive;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Module;

import java.util.ArrayList;

public abstract class BaseOpMode extends LinearOpMode {

    private static BaseOpMode instance = null;

    public BaseOpMode() {
        instance = this;
    }

    public static BaseOpMode getInstance() {
        return instance;
    }


    //----------------------------------------------------------------------------------------------
    // Runtime Variables
    //----------------------------------------------------------------------------------------------

    // Logging
    public boolean loggingEnabled = true;

    public BotcatsLog debugLog;

    public ArrayList<BotcatsLog> loggers;
    public ArrayList<Module> modules;

    // Module enablers
    public boolean driveEnabled = false;

    // Drive / Motors
    public Drive drive;

    DcMotor motorLeft;
    DcMotor motorRight;

    private ArrayList<DcMotor> leftMotors = new ArrayList<>();
    private ArrayList<DcMotor> rightMotors = new ArrayList<>();

    // Servos / Auxiliary

    // Sensors

    // Telemetry

    // Etc.

    //----------------------------------------------------------------------------------------------
    // Common Functions
    //----------------------------------------------------------------------------------------------

    // Function intended to be run at the start of any OpMode
    void initialize() {

        // Creates an ArrayList to store modules
        modules = new ArrayList<>();

        if (loggingEnabled) {
            loggers = new ArrayList<>();
            loggers.add(debugLog);
        } else {
            debugLog = new BotcatsLog(); // Disabled instance of the debug logger.
        }

        if (driveEnabled) {
            // Get the hardware devices
            motorLeft = hardwareMap.dcMotor.get("motorLeft");
            motorRight = hardwareMap.dcMotor.get("motorRight");
            
            // Add the hardware devices to the ArrayList parameters for the drive.
            leftMotors.add(motorLeft);
            rightMotors.add(motorRight);
            drive = new Drive(leftMotors, rightMotors);
        } else {
            drive = new Drive(); // Disabled instance of drive
            if (loggingEnabled) {
                debugLog.addDbgMessage(BotcatsLog.DbgLevel.INFO, "Drive", "Disabled");
            }
        }

        switch (type) {
            case TELEOP:
                // TeleOp-only module requirements can go here
            case AUTONOMOUS:
                // Auto-only module requirements can go here
        }
    }

    // Function run at the end of an OpMode. Not essential, but makes sure it exits cleanly.
    void shutdown() {
        // If the drivetrain is enabled, stop the motors.
        if (driveEnabled) {
            drive.stopAll();
        }

        // Shuts down loggers, if any.
        if (loggingEnabled && loggers.size() > 0) {
            debugLog.addDbgMessage(BotcatsLog.DbgLevel.INFO, "OpMode", "Shutting Down");
            for (BotcatsLog logger : loggers) {
                logger.closeLog();
            }
        }
    }

    enum OpModeType {
        AUTONOMOUS,
        TELEOP,
        TEST
    }

    OpModeType type;
}
