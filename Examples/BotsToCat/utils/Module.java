package org.firstinspires.ftc.teamcode.utils;

import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;

public class Module {

    protected BotcatsLog debugLogger = BaseOpMode.getInstance().debugLog; // Gets the main Logger from BaseOpMode
    public boolean moduleLoggingEnabled = BaseOpMode.getInstance().loggingEnabled; // Tells the module if logging is enabled based on whether it was enabled in BaseOpMode
    public boolean moduleEnabled = true;
    public String moduleName;

    // Shuts down a running module. This assures that OpModes and other modules will not be able to access it, stopping crashes.
    public void disableModule() {
        this.moduleEnabled = false;
        if (moduleLoggingEnabled) {
            debugLogger.addDbgMessage(
                    BotcatsLog.DbgLevel.WARN,
                    moduleName,
                    "Shutting Down"
            );
        }
    }
}
