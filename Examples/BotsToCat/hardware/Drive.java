package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.Module;

import java.util.ArrayList;

public class Drive extends Module {

    public ArrayList<DcMotor> allMotors;
    public ArrayList<DcMotor> leftMotors;
    public ArrayList<DcMotor> rightMotors;

    // Enabled constructor
    public Drive(ArrayList<DcMotor> left, ArrayList<DcMotor> right) {
        moduleName = "Drive";

        // Sets the modules left and right motors to the ones provided in BaseOpMode
        leftMotors = left;
        rightMotors = right;

        allMotors = new ArrayList<>();
        allMotors.addAll(leftMotors);
        allMotors.addAll(rightMotors);

        setDirection(leftMotors, DcMotorSimple.Direction.REVERSE);

        if (moduleLoggingEnabled) {
            debugLogger.addDbgMessage(BotcatsLog.DbgLevel.INFO, moduleName, "Initialized");
        }
    }

    // Disabled constructor
    public Drive () {
        moduleEnabled = false;
    }

    public void move(double targetPower) {
        // Like all functions, checks that the module is enabled before trying to run motors.
        // If the module is disabled, the motors are likely nonexistent or null, so it prevents a crash.
        if (moduleEnabled) {
            for (DcMotor motor : allMotors) {
                motor.setPower(targetPower);
            }
        }
    }

    public void turnLeft(double targetPower) {
        if (moduleEnabled) {
            for (DcMotor motor : leftMotors) {
                motor.setPower(targetPower);
            }
            for (DcMotor motor : rightMotors) {
                motor.setPower(-targetPower);
            }
        }
    }

    public void turnRight(double targetPower) {
        if (moduleEnabled) {
            for (DcMotor motor : leftMotors) {
                motor.setPower(-targetPower);
            }

            for (DcMotor motor : rightMotors) {
                motor.setPower(targetPower);
            }
        }
    }

    void setMode(ArrayList<DcMotor> motors, DcMotor.RunMode mode) {
        if (moduleEnabled) {
            for (DcMotor motor : motors) motor.setMode(mode);
        }
    }

    private void setPower(ArrayList<DcMotor> motors, double power) {
        if (moduleEnabled) {
            for (DcMotor motor : motors) motor.setPower(power);
        }
    }

    void setDirection(ArrayList<DcMotor> motors, DcMotor.Direction direction) {
        if (moduleEnabled) {
            for (DcMotor motor : motors) motor.setDirection(direction);
        }
    }

    public void stopAll() {
        if (moduleEnabled) {
            for (DcMotor motor : allMotors) {
                motor.setPower(speed(0));
                motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
        }
    }

}
